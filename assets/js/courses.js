const adminUser = localStorage.getItem("isAdmin")
let navbar = document.querySelector("#zuitterNav")
let header = document.querySelector("#coursesHeader")
const modalButton = document.querySelector("#adminButton")
let cardFooter; // we would use this to add a button at the bottom of each card to got to a specific course
let params = new URLSearchParams(window.location.search)
let isActive = params.get("isActive")

if(localStorage.length > 0){
	if(adminUser == "false" || !adminUser){
	navbar.innerHTML = 
		`
		<ul class="navbar-nav ml-auto">
			<li class="nav-item">
				<a href="./../index.html" class="nav-link"> Home </a>
			</li>

			<li class="nav-item active">
				<a href="./courses.html" class="nav-link"> Courses </a>
			</li>

			<li class="nav-item">
				<a href="./profile.html" class="nav-link"> Profile </a>
			</li>

			<li class="nav-item">
				<a href="./logout.html" class="nav-link"> Log Out </a>
			</li>
		</ul>
			`
	} else {

	header.innerHTML = 
				`
				<h1>Zuitter Booking Services</h1>
				<p class="lead"> Booking for everyone, everywhere </p>
				`
	navbar.innerHTML = 
		`
		<ul class="navbar-nav ml-auto">
			<li class="nav-item">
				<a href="./../index.html" class="nav-link"> Home </a>
			</li>

			<li class="nav-item active">
				<a href="./courses.html" class="nav-link"> Courses </a>
			</li>

			<li class="nav-item">
				<a href="./logout.html" class="nav-link"> Log Out </a>
			</li>

		</ul>
		`
	}
} else {
	header.innerHTML = 
				`
				<h1>Courses Offered</h1>
				<p class="lead"> Booking for everyone, everywhere </p>
				<a href="./register.html" class="btn btn-outline-primary"> Register Now! </a>
				`

	navbar.innerHTML = 
		`
		<ul class="navbar-nav ml-auto">
			<li class="nav-item">
				<a href="./../index.html" class="nav-link"> Home </a>
			</li>

			<li class="nav-item active">
				<a href="./courses.html" class="nav-link"> Courses </a>
			</li>

			<li class="nav-item">
				<a href="./register.html" class="nav-link"> Register </a>
			</li>
				
			<li class="nav-item">
				<a href="./login.html" class="nav-link"> Log in </a>
			</li>
		</ul>
		`
}


if (adminUser == "false" || !adminUser) {
	modalButton.innerHTML = null
} else {
	modalButton.innerHTML =
		`
		<div class="col-md-2 offset-md-10">
			<a href="./addCourse.html" class="btn btn-block btn-primary">
				Add Course
			</a>
		</div>
		`
}

if(adminUser == "false" || !adminUser){
	fetch("https://glacial-anchorage-43228.herokuapp.com/api/courses/active")
	.then(res => res.json())
	.then(data => {
		console.log(data)
		let courseData

		if(data.length < 1) {
			courseData = "No courses available"
		} else {
			courseData = data.map(course => { // break array into component elements
				if(localStorage.length < 1){
					cardFooter = ""
				} else {
				cardFooter = `<a href="./course.html?courseId=${course._id}" value =${course._id} class ="btn btn-primary text-white">
									Go to Course
								</a>`
				}

				return (
					`
					<div class="col-md-6 my-3">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title text-uppercase">${course.name}</h5>
								<p class="card-text text-left">
									${course.description}
								</p>
								<p class="card-text text-right">
									${course.price}
								</p>
							</div>
							<div class="card-footer">
								${cardFooter}
							</div>
						</div>
					</div>
					`
				)
			}).join("")
		}
		let container = document.querySelector("#coursesContainer")

		container.innerHTML = courseData
	})
} else {

	fetch("https://glacial-anchorage-43228.herokuapp.com/api/courses")
	.then(res => res.json())
	.then(data => {
		console.log(data)
		let courseData

		if(data.length < 1) {
			courseData = "No courses available"
		} else {
			courseData = data.map(course => { // break array into component elements
				console.log(course._id)
				console.log(course.isActive)

					if(`${course.isActive}` == "false"){
						cardFooter = `<a href="./activateCourse.html?courseId=${course._id}" value=${course._id} class = "btn btn-success text-white btn-block">
								Activate Course
								</a>`

					} else {
						cardFooter = `<a href="./deleteCourse.html?courseId=${course._id}" value=${course._id} class = "btn btn-danger text-white btn-block">
								Archive Course
								</a>`
					}

				return (
					`
					<div class="col-md-6 my-3">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title"><a href="./course.html?courseId=${course._id}" value=${course._id} class = "text-dark text-uppercase">
								${course.name}
								</a></h5>
								<p class="card-text text-left">
									${course.description}
								</p>
								<p class="card-text text-right">
									${course.price}
								</p>
							</div>
							<div class="card-footer">
								${cardFooter}
							</div>
						</div>
					</div>
					`
				)
			}).join("")
		}
		let container = document.querySelector("#coursesContainer")

		container.innerHTML = courseData
	})
}