let archiveMessage = document.querySelector("#archiveMessage")
let params = new URLSearchParams(window.location.search)
let courseId = params.get("courseId")

/*window.addEventListener('load', (event) => {
	let token = localStorage.getItem("token")

	fetch(`http://localhost:3000/api/courses/${courseId}`, {
		method: "PUT",
		headers: {
			"Content-Type" : "application/json",
			"Authorization" : `Bearer ${token}`
		},
		courseId : params.courseId
	})
  .then(res => {return res.json()})
  .then(data => {
  	console.log(data)
  	archiveMessage.innerHTML = "Course is now archived"
  })
})
*/
let token = localStorage.getItem("token")

	fetch(`https://glacial-anchorage-43228.herokuapp.com/api/courses/${courseId}/archive`, {
		method: "PUT",
		headers: {
			"Content-Type" : "application/json",
			"Authorization" : `Bearer ${token}`
		},
		courseId : params.courseId
	})
  .then(res => {return res.json()})
  .then(data => {
  	console.log(data)
  	archiveMessage.innerHTML = "Course Archived"
  })