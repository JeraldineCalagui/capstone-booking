const adminUser = localStorage.getItem("isAdmin")
const userId = localStorage.getItem("id")
const firstName = localStorage.getItem("firstname")
const lastName = localStorage.getItem("lastname")
const userEmail = localStorage.getItem("email")
const userMobileNo = localStorage.getItem("mobileNo")
/*const userCourses = localStorage.getItem("enrolledCourses")
const parsedUserCourses = JSON.parse(userCourses)*/


/*let params = new URLSearchParams(window.location.search)
let userId = params.get("userId")*/

let userName = document.querySelector("#userName")
let userNumber = document.querySelector("#userNumber")
let userEmailAdd = document.querySelector("#Email")

userName.innerHTML = `${firstName} ${lastName}`
userNumber.innerHTML = `${userMobileNo}`
userEmailAdd.innerHTML = `${userEmail}`


/*if(parsedUserCourses.length < 1){
	let container = document.querySelector("#enrolleesContainer")
		container.innerHTML = "No courses available"
} else {
	parsedUserCourses.map(course => {	
		fetch(`http://localhost:3000/api/courses/${course.courseId}`)
		.then(res => res.json())
		.then(data => {
      console.log(data)
		let container = document.querySelector("#enrolleesContainer")
		container.innerHTML +=
		
          `
          <div class="col-md-6 my-3">
            <div class="card">
              <div class="card-body">
                <h5 class="card-title text-uppercase">${data.name}</h5>
                <p class="card-text text-left">
                  ${data.description}
                </p>
                <p class="card-text text-right">

                </p>
              </div>
              <div class="card-footer">

              </div>
            </div>
          </div>
          `
         
		})
	}).join("")
} */



fetch(`https://glacial-anchorage-43228.herokuapp.com/api/users/${userId}`)
.then(res => res.json())
.then(data => {

  data.enrollments.map(courses => {
    return (
    fetch(`https://glacial-anchorage-43228.herokuapp.com/api/courses/${courses.courseId}`)
    .then(res => res.json())
    .then(data =>{
      /*console.log(data)*/
      let container = document.querySelector("#enrolleesContainer")
      container.innerHTML +=
     
         `
          <div class="col-md-6 my-3">
            <div class="card">
              <div class="card-body">
                <h5 class="card-title text-uppercase">${data.name}</h5>
                <p class="card-text text-left">
                  ${data.description}
                </p>
                <p class="card-text text-right">

                </p>
              </div>
              <div class="card-footer">

              </div>
            </div>
          </div>
          `
        
    })
    )
  }).join("")
})





   /*=>{*/
    
/*    return (
    `
          <div class="col-md-6 my-3">
            <div class="card">
              <div class="card-body">
                <h5 class="card-title text-uppercase">${courses.courseId}</h5>
                <p class="card-text text-left">
                  ${courses.description}
                </p>
                <p class="card-text text-right">

                </p>
              </div>
              <div class="card-footer">

              </div>
            </div>
          </div>
          `
          )*/
  /*}).join("")*/
  
 /* let container = document.querySelector("#enrolleesContainer")
  container.innerHTML = userCourses*/
/*})*/

/*  fetch(`http://localhost:3000/api/courses/${userCourses}`)
    .then(res => res.json())
    .then(data =>{
      console.log(data)
    })*/