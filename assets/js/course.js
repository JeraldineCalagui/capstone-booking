const adminUser = localStorage.getItem("isAdmin")
let navbar = document.querySelector("#zuitterNav")
let params = new URLSearchParams(window.location.search)
let courseId = params.get("courseId")

//JWT stored n localStorage
let token = localStorage.getItem("token")

//populate the information of a course using fetch
let courseName = document.querySelector("#courseName")
let courseDesc = document.querySelector("#courseDesc")
let coursePrice = document.querySelector("#coursePrice")
let enrollContainer = document.querySelector("#enrollContainer")
let enrollees = document.querySelector("#courseEnrollees")

console.log(adminUser)

if(adminUser == "false" || !adminUser){
  navbar.innerHTML = 
    `
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a href="./../index.html" class="nav-link"> Home </a>
      </li>

      <li class="nav-item">
        <a href="./courses.html" class="nav-link"> Courses </a>
      </li>

      <li class="nav-item">
        <a href="./profile.html" class="nav-link"> Profile </a>
      </li>

      <li class="nav-item">
        <a href="./logout.html" class="nav-link"> Log Out </a>
      </li>
    </ul>
      `
} else {
  navbar.innerHTML = 
    `
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a href="./../index.html" class="nav-link"> Home </a>
      </li>

      <li class="nav-item">
        <a href="./courses.html" class="nav-link"> Courses </a>
      </li>

      <li class="nav-item">
        <a href="./logout.html" class="nav-link"> Log Out </a>
      </li>

    </ul>
    `
}

fetch(`https://glacial-anchorage-43228.herokuapp.com/api/courses/${courseId}`)
.then(res => {return res.json()})
.then(data => {
	

	courseName.innerHTML = data.name
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price



  if(adminUser == "false" || !adminUser){ 
    enrolleesContainer.innerHTML = ""
    enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary">Enroll</button>`
    document.querySelector("#enrollButton").addEventListener("click", () => {
      fetch("https://glacial-anchorage-43228.herokuapp.com/api/users/enroll", {
        method : "POST",
          headers : {
            "Content-Type" : "application/json",
            "Authorization" : `Bearer ${token}`
          },
          body : JSON.stringify ({
          courseId : courseId
           })
      })
        .then(res => {return res.json()})
        .then(data => {
          console.log(data)
          if(data){
            alert("You have enrolled successfully")
            window.location.replace("./courses.html")
          } else {
            alert("Enrollment Failed")
          }

      })
    })
  } else {
    enrollContainer.innerHTML = `<a href="./courses.html" class="btn btn-outline-primary"> Go Back to the Courses page</a>`
    
     data.enrollees.map(enrolled => {
      console.log(enrolled.userId)
      fetch(`https://glacial-anchorage-43228.herokuapp.com/api/users/${enrolled.userId}`)
      .then(res => res.json())
      .then(data => {
        let container = document.querySelector("#enrolleesContainer")
        container.innerHTML +=
        
            `
            <div class="col-md-6 my-3">
              <div class="card">
                <div class="card-body">
                  <h5 class="card-title text-uppercase">${data.firstname} ${data.lastname}</h5>
                </div>
              </div>
            </div>
            `
          
     })
    }).join("")
    

  }
})