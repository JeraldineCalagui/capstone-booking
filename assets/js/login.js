const loginForm = document.querySelector("#logInUser")

loginForm.addEventListener("submit", (e) => {
	e.preventDefault()

	let email = document.querySelector("#userEmail").value
	let password = document.querySelector("#password").value

	if(email == "" || password == ""){
		alert("Please input email and/or password")
	} else {
		fetch("https://glacial-anchorage-43228.herokuapp.com/api/users/login", {
			method: "POST",
			headers: {
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			/*console.log(data)*/
			if(data !== false && data.access !== null){
				localStorage.setItem("token", data.access)

				fetch("https://glacial-anchorage-43228.herokuapp.com/api/users/details", {
					headers: {
						Authorization: `Bearer ${data.access}`
					}
				})
				.then(res => {
					return res.json()
				})
				.then(data => {
					let userEnrollments = data.enrollments
					console.log(userEnrollments)
					localStorage.setItem("id", data._id)
					localStorage.setItem("isAdmin", data.isAdmin)
					localStorage.setItem("firstname", data.firstname)
					localStorage.setItem("lastname", data.lastname)
					localStorage.setItem("email", data.email)
					localStorage.setItem("mobileNo", data.mobileNo)
					localStorage.setItem("enrolledCourses", JSON.stringify(userEnrollments))
										
					const adminUser = localStorage.getItem("isAdmin")
					if(adminUser == "false" || !adminUser){
						window.location.replace("./profile.html")
					} else {
						window.location.replace("./courses.html")
					}
					
				})
			} else {
				alert("Something went wrong")
			}
		})
	}
})

